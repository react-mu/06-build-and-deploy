# Build eta Deploy

> **Kontuz!** Errepositorio hau ez da deskargatzeko eta exekutatzeko, baizik eta README fitxategia irakurtzeko eta zure kabuz urratsak jarraitzeko. Beste gomendio bat, *copy-paste* egin beharrean, kodea irakurtzen duzun bitartean idaztea da.

[eu](README.eu.md) | [es](README.es.md) | [en](README.md)

* Abiapuntu moduan, sortu aplikazioa aurreko ariketan moduan:

```bash
npm create vite@4.1.0
Need to install the following packages:
create-vite@4.1.0
Ok to proceed? (y) y
✔ Project name: … react-app-build-and-deploy
✔ Select a framework: › React
✔ Select a variant: › TypeScript
```

```bash
cd react-app-build-and-deploy
npm install
npm install bootstrap
npm run dev
```

## AURKIBIDEA


* [Build](#build)
* [Deploy](#deploy)

## Build

Aplikazioa eraikitzen (*build*) badugu ```dist``` izeneko karpeta bat sortuko dugu (*distributable*):


```bash
npm run build
```

## Deploy

Aplikazioa zabalduko dugu (*deploy*) gitlab.io-n, beraz, ```.gitlab-ci.yml``` fitxategia gehitu behar dugu eduki honekin:

```yml
image: mcr.microsoft.com/devcontainers/typescript-node:1-20-bullseye
before_script:
  - cd react-app-build-and-deploy
  - npm install
pages:
  script:
    - CI=false npm run build
    - mv dist ../public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

```node:20``` irudia erabiliko dugu.

Mendekotasunak instalatuko ditugu lehenik (```npm install```) eta gero aplikazioa eraikiko (*build*) dugu. Aplikazioa eraiki ondoren, sortutako ```dist``` karpeta ```../public``` karpetara mugituko dugu, gitlab.io-n automatikoki zabalduko den artefaktu bat sortu ahal izateko.

Horrela aplikazioa gitlab orrietan argitaratuko da
[https://react-mu.gitlab.io/06-build-and-deploy](https://react-mu.gitlab.io/06-build-and-deploy).

> **Kontuz!** URL garbia edukitzeko, "Use unique domain" aukeratu gabe utzi behar dugu [https://gitlab.com/react-mu/06-build-and-deploy/pages](https://gitlab.com/react-mu/06-build-and-deploy/pages) orrialdean eta *06-build-and-deploy* ezarri base URL moduan ```vite.config.js``` fitxategian:

