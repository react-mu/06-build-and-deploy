<!---------------------------->
<!-- multilingual suffix: en, es, eu -->
<!-- no suffix: en -->
<!---------------------------->
<!-- [en] -->
# Build and Deploy

> **Warning!** This repository is not for downloading and executing, but for continuing the README and following the steps on your own. Another recomendation is to type the code as you read instead of *copy-paste*ing it.

<!-- [eu] -->
# Build eta Deploy

> **Kontuz!** Errepositorio hau ez da deskargatzeko eta exekutatzeko, baizik eta README fitxategia irakurtzeko eta zure kabuz urratsak jarraitzeko. Beste gomendio bat, *copy-paste* egin beharrean, kodea irakurtzen duzun bitartean idaztea da.

<!-- [es] -->
# Build y Deploy

> **¡Advertencia!** Este repositorio no es para descargar y ejecutar, sino para leer el README y seguir los pasos por tu cuenta. Otra recomendación es escribir el código a medida que lo lees en lugar de hacer *copy-paste*.

<!-- [common] -->
[eu](README.eu.md) | [es](README.es.md) | [en](README.md)

<!-- [en] -->
* As a starting point, create app as in previous exercise.

<!-- [eu] -->
* Abiapuntu moduan, sortu aplikazioa aurreko ariketan moduan:

<!-- [es] -->
* Como punto de partida, crea una aplicación como en el ejercicio anterior:

<!-- [common] -->
```bash
npm create vite@4.1.0
Need to install the following packages:
create-vite@4.1.0
Ok to proceed? (y) y
✔ Project name: … react-app-build-and-deploy
✔ Select a framework: › React
✔ Select a variant: › TypeScript
```

<!-- [common] -->
```bash
cd react-app-build-and-deploy
npm install
npm install bootstrap
npm run dev
```

<!-- [en] -->
## TABLE OF CONTENT

<!-- [eu] -->
## AURKIBIDEA

<!-- [es] -->
## ÍNDICE

<!-- [common] -->

* [Build](#build)
* [Deploy](#deploy)

## Build

<!-- [en] -->
If we *build* the application we will generate a folder called ```dist``` (*distributable*):

<!-- [eu] -->
Aplikazioa eraikitzen (*build*) badugu ```dist``` izeneko karpeta bat sortuko dugu (*distributable*):

<!-- [es] -->
Si construimos (*build*) la aplicación generaremos una carpeta llamada ```dist``` (distribuible):

<!-- [common] -->

```bash
npm run build
```

## Deploy

<!-- [en] -->
We will deploy the app on gitlab.io, so we have to add a ```.gitlab-ci.yml``` file with the following content:

<!-- [eu] -->
Aplikazioa zabalduko dugu (*deploy*) gitlab.io-n, beraz, ```.gitlab-ci.yml``` fitxategia gehitu behar dugu eduki honekin:

<!-- [es] -->
Desplegaremos (*deploy*) la aplicación en gitlab.io, por lo que tenemos que añadir el fichero ```.gitlab-ci.yml``` con este contenido:

<!-- [common] -->
```yml
image: mcr.microsoft.com/devcontainers/typescript-node:1-20-bullseye
before_script:
  - cd react-app-build-and-deploy
  - npm install
pages:
  script:
    - CI=false npm run build
    - mv dist ../public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

<!-- [en] -->
We are using ```node:20``` image.

We will install dependencies (```npm install```) first and then we will build the app. After the app is build, we will move the created ```dist``` folder into ```../public``` folder so we can create an artifact that will automaticaly deploy on gitlab.io.

That way we could publish the app in gitlab pages and access it via
<!-- [eu] -->
```node:20``` irudia erabiliko dugu.

Mendekotasunak instalatuko ditugu lehenik (```npm install```) eta gero aplikazioa eraikiko (*build*) dugu. Aplikazioa eraiki ondoren, sortutako ```dist``` karpeta ```../public``` karpetara mugituko dugu, gitlab.io-n automatikoki zabalduko den artefaktu bat sortu ahal izateko.

Horrela aplikazioa gitlab orrietan argitaratuko da
<!-- [es] -->
Usaremos la imagen ```node:20```.

Primero instalaremos las dependencias (```npm install```) y luego compilaremos la aplicación. Después de compilar la aplicación, moveremos la carpeta ```dist``` creada a la carpeta ```../public``` para que podamos crear un artefacto que se publicará automáticamente en gitlab.io.

De esa manera podríamos publicar la aplicación en las páginas de gitlab y acceder a ella a través de
<!-- [common] -->
[https://react-mu.gitlab.io/06-build-and-deploy](https://react-mu.gitlab.io/06-build-and-deploy).

<!-- [en] -->
> **Warning!** In order to be a clean url, we have to uncheck "Use unique domain" at [https://gitlab.com/react-mu/06-build-and-deploy/pages](https://gitlab.com/react-mu/06-build-and-deploy/pages) and add *06-build-and-deploy* as base URL at ```vite.config.js```:

<!-- [eu] -->
> **Kontuz!** URL garbia edukitzeko, "Use unique domain" aukeratu gabe utzi behar dugu [https://gitlab.com/react-mu/06-build-and-deploy/pages](https://gitlab.com/react-mu/06-build-and-deploy/pages) orrialdean eta *06-build-and-deploy* ezarri base URL moduan ```vite.config.js``` fitxategian:

<!-- [es] -->
> **¡Advertencia!** Para tener URLs limpias, tenemos que dejar "Use unique domain" sin seleccionar en [https://gitlab.com/react-mu/06-build-and-deploy/pages](https://gitlab.com/react-mu/06-build-and-deploy/pages) y añadir *06-build-and-deploy* como base URL en ```vite.config.js```:

```js
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  base: "06-build-and-deploy"
})
```
