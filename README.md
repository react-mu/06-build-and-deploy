# Build and Deploy

> **Warning!** This repository is not for downloading and executing, but for continuing the README and following the steps on your own. Another recomendation is to type the code as you read instead of *copy-paste*ing it.

[eu](README.eu.md) | [es](README.es.md) | [en](README.md)

* As a starting point, create app as in previous exercise.

```bash
npm create vite@4.1.0
Need to install the following packages:
create-vite@4.1.0
Ok to proceed? (y) y
✔ Project name: … react-app-build-and-deploy
✔ Select a framework: › React
✔ Select a variant: › TypeScript
```

```bash
cd react-app-build-and-deploy
npm install
npm install bootstrap
npm run dev
```

## TABLE OF CONTENT


* [Build](#build)
* [Deploy](#deploy)

## Build

If we *build* the application we will generate a folder called ```dist``` (*distributable*):


```bash
npm run build
```

## Deploy

We will deploy the app on gitlab.io, so we have to add a ```.gitlab-ci.yml``` file with the following content:

```yml
image: mcr.microsoft.com/devcontainers/typescript-node:1-20-bullseye
before_script:
  - cd react-app-build-and-deploy
  - npm install
pages:
  script:
    - CI=false npm run build
    - mv dist ../public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

We are using ```node:20``` image.

We will install dependencies (```npm install```) first and then we will build the app. After the app is build, we will move the created ```dist``` folder into ```../public``` folder so we can create an artifact that will automaticaly deploy on gitlab.io.

That way we could publish the app in gitlab pages and access it via
[https://react-mu.gitlab.io/06-build-and-deploy](https://react-mu.gitlab.io/06-build-and-deploy).

> **Warning!** In order to be a clean url, we have to uncheck "Use unique domain" at [https://gitlab.com/react-mu/06-build-and-deploy/pages](https://gitlab.com/react-mu/06-build-and-deploy/pages) and add *06-build-and-deploy* as base URL at ```vite.config.js```:

