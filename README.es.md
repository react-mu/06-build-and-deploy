# Build y Deploy

> **¡Advertencia!** Este repositorio no es para descargar y ejecutar, sino para leer el README y seguir los pasos por tu cuenta. Otra recomendación es escribir el código a medida que lo lees en lugar de hacer *copy-paste*.

[eu](README.eu.md) | [es](README.es.md) | [en](README.md)

* Como punto de partida, crea una aplicación como en el ejercicio anterior:

```bash
npm create vite@4.1.0
Need to install the following packages:
create-vite@4.1.0
Ok to proceed? (y) y
✔ Project name: … react-app-build-and-deploy
✔ Select a framework: › React
✔ Select a variant: › TypeScript
```

```bash
cd react-app-build-and-deploy
npm install
npm install bootstrap
npm run dev
```

## ÍNDICE


* [Build](#build)
* [Deploy](#deploy)

## Build

Si construimos (*build*) la aplicación generaremos una carpeta llamada ```dist``` (distribuible):


```bash
npm run build
```

## Deploy

Desplegaremos (*deploy*) la aplicación en gitlab.io, por lo que tenemos que añadir el fichero ```.gitlab-ci.yml``` con este contenido:

```yml
image: mcr.microsoft.com/devcontainers/typescript-node:1-20-bullseye
before_script:
  - cd react-app-build-and-deploy
  - npm install
pages:
  script:
    - CI=false npm run build
    - mv dist ../public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

Usaremos la imagen ```node:20```.

Primero instalaremos las dependencias (```npm install```) y luego compilaremos la aplicación. Después de compilar la aplicación, moveremos la carpeta ```dist``` creada a la carpeta ```../public``` para que podamos crear un artefacto que se publicará automáticamente en gitlab.io.

De esa manera podríamos publicar la aplicación en las páginas de gitlab y acceder a ella a través de
[https://react-mu.gitlab.io/06-build-and-deploy](https://react-mu.gitlab.io/06-build-and-deploy).

> **¡Advertencia!** Para tener URLs limpias, tenemos que dejar "Use unique domain" sin seleccionar en [https://gitlab.com/react-mu/06-build-and-deploy/pages](https://gitlab.com/react-mu/06-build-and-deploy/pages) y añadir *06-build-and-deploy* como base URL en ```vite.config.js```:

```js
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  base: "06-build-and-deploy"
})
```
